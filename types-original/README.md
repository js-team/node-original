# Installation
> `npm install --save @types/original`

# Summary
This package contains type definitions for original (https://github.com/unshiftio/original).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/original

Additional Details
 * Last updated: Tue, 18 Dec 2018 16:48:01 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by Wayne Carson <https://github.com/wcarson>.
